
***********
* README: *
***********

DESCRIPTION:
------------
This module provide a facet for Faceted Search to browse Ubercart products for prices range.
The module does not require any configuration.

REQUIREMENTS:
-------------
UC Price Facet requires Faceted Search and Ubercart.

INSTALLATION:
-------------
1. Place the entire uc_price_facet directory into your Drupal sites/all/modules/
   directory.

2. Enable the uc_price_facet module


Author:
-------
psegno
info@psegno.it
francesco.pesenti@psegno.it