<?php
// $Id $

/**
 * @file
 * Provides a facet for searching content by UC Price.
 */

require_once('./'. drupal_get_path('module', 'faceted_search') .'/faceted_search.inc');

/**
 * Implementation of hook_faceted_search_collect().
 */
function uc_price_facet_faceted_search_collect(&$facets, $domain, $env_id, $selection, $arg = NULL) {
  switch ($domain) {
    case 'facets':
      // If the Location facet is allowed.
      if (!isset($selection) || isset($selection['uc_price'][1])) {
        $facets[] = new uc_price_facet();
      }
      break;
    /**
     * Mostra le faccette in base alla selezione corrente
     */
    case 'text':
      // If the date authored facet is allowed.
      if (!isset($selection) || isset($selection['uc_price'][1])) {
        if ($uc_price = search_query_extract($arg, 'uc_price')) {
          $facets[] = new uc_price_facet($uc_price);
          $arg = search_query_insert($arg, 'uc_price');
        }
      }
      return $arg;
  }
}

/**
 * A facet for searching content by Location.
 */
class uc_price_facet extends faceted_search_facet {  
  var $_uc_price = NULL;
  
  /**
   * Constructor.
   */
  function uc_price_facet($uc_price = NULL, $active_path = array()) {
    if ($uc_price) {
      $active_path[] = new uc_price_facet_category($uc_price);
    }
    parent::faceted_search_facet('uc_price', $active_path);
  }

  function get_id() {
    return 1; // This module provides only one facet.
  }

  function get_label() {
    return t('Price');
  }

  /**
   * Returns the available sort options for this facet.
   */
  function get_sort_options() {
    $options = parent::get_sort_options();
    return $options;
  }

  /**
   * Handler for the 'count' sort criteria.
   */
  function build_sort_query_count(&$query) {
    $query->add_orderby('count', 'DESC');
    $query->add_orderby('sell_price', 'DESC');
  }

  /**
   * Returns the search text for this facet, taking into account this facet's
   * active path.
   */
  function get_text() {
    if ($category = $this->get_active_category()) {
      return $category->get_text();
    }
    return '';
  }
  
  /**
   * Updates a query for retrieving the root categories of this facet and their
   * associated nodes within the current search results. 
   *
   * @param $query
   *   The query object to update.
   *
   * @return
   *   FALSE if this facet can't have root categories.
   */
  function build_root_categories_query(&$query) {
    $max_price = db_result(db_query("SELECT MAX(sell_price) FROM {uc_products}"));
    $divider = variable_get('uc_price_facet_divider', 50);
    $query->add_table('uc_products', 'nid', 'n', 'nid');
    for ($i = 0; $i < $max_price / $divider; $i++) {
      $min = $i * $divider;
      $max = $min + $divider;
      $query->add_field(NULL, 'sum(case when sell_price BETWEEN '. $min .' AND '. $max .' then 1 else 0 end)', '"'. $min .','. $max .'"');
    }    
    return TRUE;
  }
  
  /**
   * This factory method creates categories given query results that include the
   * fields selected in get_root_categories_query() or get_subcategories_query().
   *
   * @param $results
   *   $results A database query result resource.
   *
   * @return
   *   Array of categories.
   */
  function build_categories($results) {
    $categories = array();
    while ($result = db_fetch_array($results)) {
      foreach (array_keys($result) as $range) {
        $count = $result[$range];
        // TODO: eliminare count dai field
        if ($range != 'count' && $count > 0) {
          $categories[] = new uc_price_facet_category($range, $count);
        }
      }
    }
    return $categories;
  }
}


/**
 * A category for Location.
 */
class uc_price_facet_category extends faceted_search_category {
  var $_uc_price = NULL;
  
  /**
   * Constructs a category for the specified date.
   *
   * @param $year
   *   Year corresponding to this category.
   *
   * @param $month
   *   Month corresponding to this category. Optional, but must be specified if
   *   $day is specified.
   *
   * @param $day
   *   Day corresponding to this category. Optional.
   *
   * @param $count
   *   The number of nodes associated to this category within the current
   *   search. Optional.
   * 
   * Note: We consider the specified date as within the user's timezone.
   */
  function uc_price_facet_category($uc_price, $count = NULL) {
    parent::faceted_search_category($count);
    $this->_uc_price = $uc_price;
  }

  /**
   * Return the label of this category.
   *
   * @param $html
   *   TRUE when HTML is allowed in the label, FALSE otherwise. Checking this
   *   flag allows implementors to provide a rich-text label if desired, and an
   *   alternate plain text version for cases where HTML cannot be used. The
   *   implementor is responsible to ensure adequate security filtering.
   */
  function get_label($html = FALSE) {
    $sign = variable_get('uc_currency_sign', '$');
    $position = variable_get('uc_sign_after_amount', FALSE);
    $prices = split(',', $this->_uc_price);
    $label = $prices[0] .' - '. $prices[1];
    if ($position) {
      return $label . $sign;
    }
    else {
      return $sign . $label;
    }
  }

  function get_text() {
    return $this->_uc_price;
  }

  /**
   * Updates a query for selecting nodes matching this category.
   *
   * @param $query
   *   The query object to update.
   */
  function build_results_query(&$query) {
    $prices = split(',', $this->_uc_price);
    $query->add_table('uc_products', 'nid', 'n', 'nid');
    $query->add_field(NULL, 'sell_price', 'uc_price');
    $query->add_where("uc_products.sell_price BETWEEN %d AND %d", $prices[0], $prices[1]);
  }
}
